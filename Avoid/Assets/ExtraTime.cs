﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtraTime : MonoBehaviour
{

    public float extraTime;
    public float movespeed;
    public float upVelocity;
    public Rigidbody2D rb;
    public float multiplier;
    private float dirX;
    private float dirY;
    public float left, right, top, bottom;
    public Collider2D col;

    private void Awake()
    {
        GameManager.instance.bonusOnScene = true;
    }

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        

        
            StartCoroutine("Dissapear");
      
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.timer <= 0)
        {
            this.gameObject.SetActive(false);
        }
    }

    private void FixedUpdate()
    {
         dirX = movespeed;
         dirY = upVelocity;

         if (GameManager.instance.ready)
         {
             rb.velocity = new Vector2(dirX * multiplier, dirY * multiplier);
             rb.gravityScale = 1f;
         }
         else
         {
             rb.velocity = Vector2.zero;
             rb.gravityScale = 0;
         }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (this.gameObject.CompareTag("BonusCatch"))
            {
                GameManager.instance.AddTime(extraTime);
             
                col.enabled = false;
                GameManager.instance.bonusOnScene = false;
                this.gameObject.SetActive(false);
            }

            if (this.gameObject.CompareTag("Multiply"))
            {
                GameManager.instance.multiply = true;
                col.enabled = false;
                GameManager.instance.bonusOnScene = false;
                this.gameObject.SetActive(false);
            }

            if (this.gameObject.CompareTag("Recover"))
            {
                GameManager.instance.recoverScore();
                
                col.enabled = false;
                GameManager.instance.bonusOnScene = false;
                this.gameObject.SetActive(false);
            }






        }
    }

     private void OnCollisionEnter2D(Collision2D collision)
        {
            Vector2 newPos = transform.position;

            if (collision.gameObject.CompareTag("LeftBorder"))
            {

                

                newPos.x = right;
                multiplier += 0.001f;

            }
            if (collision.gameObject.CompareTag("RightBorder"))
            {

            

                newPos.x = left;
                multiplier += 0.001f;
            }

            if (collision.gameObject.CompareTag("TopBorder"))
            {


                newPos.y = bottom;
           
                multiplier += 0.001f;
            }

            if (collision.gameObject.CompareTag("BottomBorder"))
            {


                newPos.y = top;
              
                multiplier += 0.001f;
            }

        transform.position = newPos;
    }


         IEnumerator Dissapear()
        {
             yield return new WaitForSeconds(5.5f);
            GameManager.instance.bonusOnScene = false;
            this.gameObject.SetActive(false);
        }
}
