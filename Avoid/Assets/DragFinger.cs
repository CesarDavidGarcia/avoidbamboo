﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragFinger : MonoBehaviour
{

    private Vector3 touchPosition;
    public Rigidbody2D rb;
    private Vector3 direction;
    private float moveSpeed = 15;
    public bool isTouchableArea;
    

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        //moveSpeed = 10;
    }

    // Update is called once per frame
    void Update()
    {

        if(GameManager.instance.timer > 0 && GameManager.instance.ready)
        {
            if (Input.touchCount > 0)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                //Debug.Log(ray.origin);

                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {
                    //Debug.Log("Ray Catsed");
                    if (hit.collider != null && hit.collider.name == "Quad")
                    {
                        //Debug.Log(hit.collider.name);
                        isTouchableArea = true;

                    }

                }
                else
                {
                    isTouchableArea = false;
                }

                if (isTouchableArea)
                {
                    Touch touch = Input.GetTouch(0);
                    touchPosition = Camera.main.ScreenToWorldPoint(touch.deltaPosition);
                    touchPosition.z = 0;
                    direction = (touchPosition - transform.position);
                    rb.velocity = new Vector3(direction.x - 3f, direction.y - 3f) * moveSpeed;

                    /* if (touch.phase == TouchPhase.Ended)
                     {
                         rb.velocity = Vector2.zero;
                     }*/
                }



            }
        }
       
    }
}
