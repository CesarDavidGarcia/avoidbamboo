﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor.Experimental.AssetImporters;
using UnityEngine;
using UnityEngine.XR;

public class PlayerBehaviour : MonoBehaviour
{

    public SpriteRenderer rendererPlayer;
    public Collider2D colliderPlayer;
    public float left, right, top, bottom;

    // Start is called before the first frame update


    private void Start()
    {
        rendererPlayer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if(GameManager.instance.timer <= 0)
        {
            colliderPlayer.enabled = false;
            rendererPlayer.color = new Color32(0, 0, 0, 150);
            StopAllCoroutines();
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Hazard"))
        {
         
            GameManager.instance.minusScore();
            GameManager.instance.wasHit = true;
            StartCoroutine(DoBlinks(0.7f, 0.7f));
            Handheld.Vibrate();
        }
    }


    IEnumerator DoBlinks(float duration, float blinkTime)
    {
        
        while (duration > 0f)
        {
            duration /= 10;
            duration -= Time.deltaTime;

          
            rendererPlayer.color = new Color32(0, 0, 0, 150);
            colliderPlayer.enabled = false;

            //wait for a bit
            yield return new WaitForSeconds(blinkTime);
        }

    
       
        rendererPlayer.color = new Color32(255, 255, 255, 255);
        colliderPlayer.enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Vector2 newPos = transform.position;

        if (collision.gameObject.CompareTag("LeftBorder"))
        {

            

            newPos.x = right;
            

        }
        if (collision.gameObject.CompareTag("RightBorder"))
        {

            

            newPos.x = left;
            
        }

        if (collision.gameObject.CompareTag("TopBorder"))
        {


            newPos.y = bottom;
            
           
        }

        if (collision.gameObject.CompareTag("BottomBorder"))
        {


            newPos.y = top;
         
           
        }

        

        transform.position = newPos;
    }
}

